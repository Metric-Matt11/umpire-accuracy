import datetime

from scipy.stats import linregress
import statsmodels as stat
import pandas as pd
import matplotlib.pyplot as plt
from pybaseball import schedule_and_record
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error
from sklearn.ensemble import RandomForestRegressor
import numpy as np
import datetime as DT
import requests
import urllib.request
import time
from bs4 import BeautifulSoup


def web_scrape(url, headings):
    response = requests.get(url).content
    response = str(response).encode()
    soup = BeautifulSoup(response, features="lxml")
    table = soup.find_all('table')[0]
    data = []
    data.append(headings)
    table_body = table.find('tbody')
    rows = table_body.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        row_anchor = row.find("a")
        mlbid = row_anchor["href"].split("mlb_ID=")[-1] if row_anchor else pd.NA  # ID str or nan
        cols = [ele.text.strip() for ele in cols]
        cols.append(mlbid)
        data.append([ele for ele in cols])
    df = pd.DataFrame(data)
    df = df.rename(columns=df.iloc[0])
    df = df.reindex(df.index.drop(0))
    df = df.dropna()
    return df


Current_players = web_scrape(
    'https://www.baseball-reference.com/leagues/daily.fcgi?user_team=&bust_cache=&type=b&lastndays=7&dates'
    '=fromandto&fromandto={}.{}&level=mlb&franch=&stat=&stat_value=0', ['Name', '', 'Age', '#days', 'Lev',
                                                                        'Tm', 'G', 'PA', 'AB', 'R', 'H', '2B',
                                                                        '3B', 'HR', 'RBI', 'BB', 'IBB', 'SO',
                                                                        'HBP', 'SH', 'SF', 'GDP', 'SB', 'CS',
                                                                        'BA', 'OBP', 'SLG', 'OPS', 'mlbID'])

MVP_Winners = web_scrape('https://www.baseball-reference.com/awards/mvp.shtml',
                         ['Lg', 'Name', 'Tm', 'WAR', 'BA', 'OBP', 'SLG'
                             , 'HR', 'RBI', 'SB', 'W', 'L', 'SV', 'ERA', 'IP', 'SO'])

current_mvps = pd.merge(MVP_Winners, Current_players, on='Name', how='inner').drop_duplicates('Name')
current_mvps['team_lg'] = current_mvps.Lev + ' ' + current_mvps.Tm_y

parks = pd.read_csv('datasets/Park_Mapping.csv')
parks_loc = parks[['Team Abv', 'Arena Location', 'Seating Capacity', 'team_lg']]
games = pd.read_csv('datasets/umpgames.csv')
umpires = pd.read_csv('datasets/umpstats.csv')
time_record_by_game = pd.DataFrame()

games['Date_team'] = games['Date'] + games['Home Team']
parks_list = parks['Team Abv'].tolist()

# Code snipet to clean date col and put in other tables format
for teams in parks_list:
    time_and_record = schedule_and_record(2021, teams)
    time_record_by_game = time_record_by_game.append(time_and_record)

# Adding Columns
time_record_by_game['Doubleheader Game'] = time_record_by_game['Date'].apply(
    lambda dh: dh[dh.find("(") + 1:dh.find(")")] if dh.find(')') > 0 else 0)
time_record_by_game['Date'] = time_record_by_game['Date'].map(lambda date: date[:-4] if date.find(')') > 0 else date)
time_record_by_game['Date_team'] = time_record_by_game['Date'] + time_record_by_game['Tm']
time_record_by_game = time_record_by_game[(time_record_by_game['D/N'] == 'D') | (time_record_by_game['D/N'] == 'N')]
time_record_by_game['GB'] = time_record_by_game['GB'].replace(['Tied'], 0)
time_record_by_game['GB'] = time_record_by_game['GB'].str.replace('up ', '-')

# Creating index of date, home team and Opp so I can match away and home team data for GB column
time_record_by_game2 = time_record_by_game[['Date', 'Tm', 'Opp', 'Doubleheader Game', 'Rank']]
time_record_by_game['game_index'] = time_record_by_game['Date'] + time_record_by_game['Tm'] + \
                                    time_record_by_game['Opp'] + time_record_by_game['Doubleheader Game'].astype(str)
time_record_by_game2['game_index'] = time_record_by_game2['Date'] + time_record_by_game2['Opp'] + \
                                     time_record_by_game2['Tm'] + time_record_by_game2['Doubleheader Game'].astype(str)
time_record_by_game = pd.merge(time_record_by_game, time_record_by_game2[['Rank', 'game_index']], on='game_index',
                               suffixes=('_Home_team', '_Away_team'))

# The lower the Avg Rank, the more intense the game
time_record_by_game['Avg Rank'] = (time_record_by_game.Rank_Home_team + time_record_by_game.Rank_Away_team) / 2

umpire_games = pd.merge(games, umpires, how='inner', on='Umpire', suffixes=("", "_season"))
umpire_games_parks = pd.merge(umpire_games, parks_loc, how='inner', left_on='Home Team', right_on='Team Abv')
umpire_games_parks_time = pd.merge(umpire_games_parks, time_record_by_game, how='inner', on='Date_team')
umpire_games_parks_time = pd.merge(umpire_games_parks_time, current_mvps[['team_lg', 'Name']], on='team_lg', how='left')
umpire_games_parks_time = umpire_games_parks_time.drop_duplicates(subset=['ID'])

# Formatting certain columns as booleans
Night_game_mapping = {'D': False, 'N': True}
umpire_games_parks_time['MVP_flg'] = umpire_games_parks_time['Name'].apply(lambda x: 0 if pd.isnull(x) else 1)
# Season_list = {''}
umpire_games_parks_time['is_Night_game'] = umpire_games_parks_time['D/N'].map(Night_game_mapping)
umpire_games_parks_time['Time'] = pd.to_datetime(umpire_games_parks_time['Time'])

usable_cols = ['Date_x', 'Umpire', 'Home Team', 'Away Team', 'Home Score', 'Away Score', 'Accuracy',
               'Consistency', 'Favor [Home]', 'Games', 'Called Pitches_season', 'Accuracy_season',
               'Arena Location', 'W/L', 'R', 'Inn', 'GB', 'Time', 'is_Night_game', 'Attendance', 'Streak',
               'Doubleheader Game', 'Avg Rank', 'MVP_flg']

usable_data = umpire_games_parks_time[usable_cols]

matrix = usable_data.corr()

model_cols = ['Games', 'Called Pitches_season', 'Accuracy_season',
              'GB', 'Attendance', 'Streak',
              'Doubleheader Game', 'Avg Rank', 'is_Night_game', 'MVP_flg']

model_data = umpire_games_parks_time[model_cols]
model_data = model_data.fillna(0)

def plot_scatter(x_axis, y_axis, input_data, color_col=None):
    plt.scatter(x_axis, y_axis, data=input_data, c=color_col)
    plt.xlabel(x_axis)
    plt.ylabel(y_axis)
    plt.show()


# Linear Corr testing
Correlation_Matrix = usable_data.corr()

# Non Linear Corr testing
reg = linregress(usable_data.Attendance, usable_data.Accuracy)
fx = np.array([usable_data.Attendance.min(), usable_data.Attendance.max()])
fy = reg.intercept + reg.slope * fx
plt.plot(fx, fy, '-')

# Predictive Rand Forest model
x = model_data
y = usable_data.Accuracy
train_x, val_x, train_y, val_y = train_test_split(x, y, random_state=2)
performance_variables = []
umpire_performance_model = RandomForestRegressor(random_state=2)
umpire_performance_model.fit(train_x, train_y)
umpire_predictions = umpire_performance_model.predict(val_x)
print(umpire_predictions)
print(mean_absolute_error(val_y, umpire_predictions))
